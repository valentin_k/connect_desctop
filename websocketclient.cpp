#include "websocketclient.h"

#include <QtCore/QDebug>

QT_USE_NAMESPACE

websocketclient::websocketclient(const QUrl &url, bool debug, QObject *parent) :
    QObject(parent),
    Murl(url),
    Mdebug(debug),
    qr(QUrl(QStringLiteral("")))
{
    if (Mdebug)
        qDebug() << "WebSocket server:" << url;
    connect(&MwebSocket, &QWebSocket::connected, this, &websocketclient::onConnected);
    connect(&MwebSocket, &QWebSocket::disconnected, this, &websocketclient::closed);
    MwebSocket.open(QUrl(url));
}

void websocketclient::onConnected()
{
    if (Mdebug)
        qDebug() << "WebSocket connected";
    connect(&MwebSocket, &QWebSocket::textMessageReceived,
            this, &websocketclient::onTextMessageReceived);
}

QJsonObject websocketclient::ObjectFromString(QString in)
{
    QJsonObject obj;

    QJsonDocument doc = QJsonDocument::fromJson(in.toUtf8());

    // check validity of the document
    if(!doc.isNull())
    {
        if(doc.isObject())
        {
            obj = doc.object();
        }
        else
        {
            qDebug() << "Document is not an object" << endl;
        }
    }
    else
    {
        qDebug() << "Invalid JSON...\n" << in << endl;
    }

    return obj;
}

 QString websocketclient::StringFromObject(const QJsonObject& in)  // to do
{
     QJsonDocument doc(in);
     QString strJson(doc.toJson(QJsonDocument::Compact));

    return strJson;
}

void websocketclient::onTextMessageReceived(QString message)
{
    if (Mdebug)
        qDebug() << "recieved  " << message;

     QJsonObject incoming=ObjectFromString(message);

    QString type=incoming["type"].toString();

    if (type=="technical")
    {
        if (Mdebug)
            qDebug() << "code" << incoming["code"].toInt() <<" message "<<incoming["text"].toString();
    }

     if (type=="id_respond")
    {
        id=incoming["room_id"].toString();
        qr=QUrl(incoming["qr"].toString() );

        emit qr_recieved();
    }
     if (type=="message")
    {
        parceline=incoming["body"].toString();

        emit parceline_recieved();
    }
}



void websocketclient::sendMessage(QString text)
{
    QJsonObject message;

    message["type"]=QStringLiteral("message");
    message["body"]=text;

    MwebSocket.sendTextMessage(StringFromObject(message));
}

QUrl websocketclient::getQRAdress()
{
    return qr;
}

void websocketclient::createRoom()
{
    MwebSocket.sendTextMessage(QStringLiteral("{\"type\": \"sign_up\"}"));
}

void websocketclient::connectToRoom(QString rooMid)
{
    QJsonObject message;

    message["type"]=QStringLiteral("login");
    message["room_id"]=rooMid;

    MwebSocket.sendTextMessage(StringFromObject(message));
}

QString websocketclient::getRoomId()
{
    return id;
}

QString websocketclient::getParceline()
{
    return parceline;
}
