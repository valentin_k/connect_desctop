#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCloseEvent>
#include <QUrl>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this -> setTrayIconActions();
    this -> showTrayIcon();
    //this -> setFormStyle();
    this->activateClipBoard();

    client=new websocketclient(QUrl(QStringLiteral("ws://localhost:8765")), true,this);
    //client=new websocketclient(QUrl(QStringLiteral("ws://62.109.14.225:8765")), true,this);

    QObject::connect(client, SIGNAL (parceline_recieved()), this, SLOT (parcelineRecieved()) );
    QObject::connect(client, SIGNAL (qr_recieved()), this, SLOT (qrRecieved()) );

    clipBoardChangedByQT=false;
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::activateClipBoard()
{
    board=QApplication::clipboard();
    connect (board, SIGNAL(dataChanged()), this, SLOT(on_clipboard_changed()));
}

void MainWindow::showTrayIcon()
{
    // Setting tray icon...
    trayIcon = new QSystemTrayIcon(this);
    QIcon trayImage(":/cnct.png");
    trayIcon -> setIcon(trayImage);

    trayIcon -> setContextMenu(trayIconMenu);

    // Connecting actions to our icon...
    //connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

    trayIcon -> show();
}

void MainWindow::setTrayIconActions()
{
    // Setting actions...
    minimizeAction = new QAction("Minimize", this);
    restoreAction = new QAction("Restore", this);
    quitAction = new QAction("Quit", this);

    // Connecting actions to slots...
    connect (minimizeAction, SIGNAL(triggered()), this, SLOT(hideWindow()));
    //connect (restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));
    connect (restoreAction, SIGNAL(triggered()), this, SLOT(restoreWindow()));
    connect (quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

    // Setting system tray's icon menu...
    trayIconMenu = new QMenu(this);

    trayIconMenu -> addAction (quitAction);
    trayIconMenu -> addAction (minimizeAction);    
}

void MainWindow::restoreWindow ()
{
    trayIconMenu -> addAction (minimizeAction);
    trayIconMenu -> removeAction(restoreAction);

    this->showNormal();
}

void MainWindow::hideWindow ()
{
    trayIconMenu -> addAction (restoreAction);
    trayIconMenu -> removeAction(minimizeAction);
    this->hide();
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    event->ignore();
    trayIconMenu -> addAction (restoreAction);
    trayIconMenu -> removeAction(minimizeAction);
    this->hide();
}

void MainWindow::on_clipboard_changed()
{
    if (clipBoardChangedByQT)
    {
        clipBoardChangedByQT=false;
    }
    else
    {
        client->sendMessage(board->text());
    }
    //ui->label->setText(board->text() );
    //board->setText(ui->lineEdit->text() );
}

void MainWindow::parcelineRecieved()
{
    qDebug() << "parceline recieved!" << client->getParceline();
    clipBoardChangedByQT=true;
    board->setText(client->getParceline());
}

void MainWindow::qrRecieved()
{
    QUrl imageUrl=client->getQRAdress();
    MImgCtrl = new FileDownloader(imageUrl, this);

    connect(MImgCtrl, SIGNAL (downloaded()), this, SLOT (loadImage()));
}

void MainWindow::loadImage()
{
 QPixmap qrImage;
 qrImage.loadFromData(MImgCtrl->downloadedData());

 ui->label->setPixmap(qrImage);
 ui->lineId->setText(client->getRoomId());
}

void MainWindow::on_connectButton_clicked()
{
    client->connectToRoom(ui->lineId->text());
}

void MainWindow::on_createButton_clicked()
{
    client->createRoom();
}
