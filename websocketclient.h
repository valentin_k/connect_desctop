#ifndef WEBSOCKETCLIENT_H
#define WEBSOCKETCLIENT_H

#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include <QJsonDocument>
#include <QJsonObject>

class websocketclient : public QObject
{
    Q_OBJECT
public:
    explicit websocketclient(const QUrl &url, bool debug = false, QObject *parent = Q_NULLPTR);

Q_SIGNALS:
    void closed();

signals:
    void parceline_recieved();
    void qr_recieved();

private Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);
    QJsonObject ObjectFromString(QString in);
    QString StringFromObject(const QJsonObject& in);

public Q_SLOTS:
    void sendMessage(QString text);
    QUrl getQRAdress();
    QString getRoomId();
    QString getParceline();
    //QString parceline(QString text);

    void createRoom();

    void connectToRoom(QString room_id);


private:
    QWebSocket MwebSocket;
    QUrl Murl;
    bool Mdebug;
    QUrl qr;
    QString id;
    QString parceline;
};

#endif // WEBSOCKETCLIENT_H
