#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QClipboard>

#include "websocketclient.h"
#include "filedownloader.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QSystemTrayIcon *trayIcon;

    QMenu *trayIconMenu;

    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *quitAction;
    QClipboard *board;

    websocketclient *client;

    FileDownloader *MImgCtrl;

    bool clipBoardChangedByQT;

private Q_SLOTS:
    void restoreWindow ();
    void hideWindow ();

private slots:
    void showTrayIcon();
    void activateClipBoard();
    void setTrayIconActions();
    void closeEvent(QCloseEvent *event);
    void on_clipboard_changed();
    void loadImage();
    void on_connectButton_clicked();

    void on_createButton_clicked();

public slots:
    void parcelineRecieved();
    void qrRecieved();

};

#endif // MAINWINDOW_H
